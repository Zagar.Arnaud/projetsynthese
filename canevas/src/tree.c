#include <stdlib.h>
#include <stdio.h>
#include "tree.h"

static void preorder(EventNode *node) {
  if (node != NULL) {
    display_point(node->key);
    printf("\n");
    preorder(node->left);
    preorder(node->right);
  }
}

static void inorder(EventNode *node) {
  if (node != NULL) {
    inorder(node->left);
    display_point(node->key);
    printf("\n");
    inorder(node->right);
  }
}

static void postorder(EventNode *node) {
  if (node != NULL) {
    postorder(node->left);
    postorder(node->right);
    display_point(node->key);
    printf("\n");
  }
}

// order = 0 (preorder), 1 (inorder), 2 (postprder)
void display_tree_keys(const EventTree *tree, int order) {
  switch (order) {
    case 0:
      preorder(tree->root);
      break;
    case 1:
      inorder(tree->root);
      break;
    case 2:
      postorder(tree->root);
      break;
    default:
      printf("display_tree_keys: non valid order parameter\n");
      exit(1);
  }
}

/*
 * renvoie un nouveau EventNode d'attribut
 *  <key, type, s1, s2, NULL, NULL>
 */
EventNode * new_event(Point key, int type, Segment *s1, Segment *s2) {
  
  EventNode *t = (EventNode*)malloc(sizeof(EventNode));
  t->key = key;
  t->type = type;
  
  t->s1 = s1;
  t->s2 = s2;
  
  t->left = NULL;
  t->right = NULL;

  return t;


}

/*
 * recherche la place dans tree du nouvel événement event
 * et l'insère.
 * L'ABR est modifié et la modification peut porter sur sa racine.
 */
void insert_event(EventTree *tree, EventNode *event) {
	
if (tree->size == 0)
		{tree->root = event;}
	else{
		
		EventNode * Actuel = tree->root;
		int compt= 1;
		while(compt){
			if (point_prec(event->key, Actuel->key)){
		
				if(Actuel->left != NULL)
					{Actuel = Actuel->left;}
				else
					{compt = 0;}
			}
			else{
			
				if (point_prec(Actuel->key, event->key)){ 
					if(Actuel->right != NULL)
						{Actuel = Actuel->right;}
					else
						{compt = 0;}				
				}
			}
		
		}
			
		if(point_prec(event->key, Actuel->key))
			{Actuel->left = event;}
		else
			{Actuel->right = event;}
	}
	tree->size++;
	
}


/*
 * trouve le prochain événement, le supprime de l'arbre et le renvoie
 */
EventNode* get_next_event(EventTree *tree) {

	EventNode * nactual = tree->root;
	EventNode * nroot = tree->root;

	while(nactual->left != NULL){
		nroot =  nactual;
		nactual =  nactual->left;
	}
	if(nactual != tree->root)
		{nroot->left = nactual->right;}
	else
		{tree->root = tree->root->right;}
	tree->size=tree->size - 1;
	
	return(nactual);



}

/*
 * renvoie 1 si le clef key existe dans l'ABR tree, O sinon
 */
int event_exists(EventTree *tree, Point key) {
 
	EventNode * nactual = tree->root;
 
	int compt;
	while(compt){
		if(point_prec(key, nactual->key)){ 
			if(nactual->left != NULL)
				{nactual = nactual->left;}
			else
				{compt = 0;}
		}
		else{
			if(point_prec(nactual->key, key)){ 
				if(nactual->right != NULL)
					{nactual = nactual->right;}
				else
					{compt = 0;}
			}
			else
				{return(1);}
		}
	}
	return(0);	

}

