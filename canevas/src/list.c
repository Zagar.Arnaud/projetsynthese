#include <stdlib.h>
#include <stdio.h>
#include "list.h"

/*
 * créer et renvoie un nouveau nœud (LNode)
 */
static LNode * new_node(void *data) {
	LNode* n;
	
	n = (LNode *)malloc(sizeof(LNode));
	n -> data =data;
	n->next = NULL;
	n->prev = NULL;
	return n;
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en tête de la liste *list
 */
void list_prepend(List *list, void *data) {

	LNode *n = new_node(data);
	if(list->tail != NULL){
		list->head->prev = n;
		n->next = list->head;
		list->head = n;
		list->size++;
	}
	else{
		list->tail = n;
		list->head = n;
		list->size++;
	}

	
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément en queue de la liste *list
 */
void list_append(List *list, void *data) {
	
	LNode *n = new_node(data);

	if(list->tail == NULL){
		list->tail = n;
		list->head = n;
	}
	else{
		list->tail->next = n;
		n->prev = list->tail;
		list->tail = n;
	}
	list -> size++;
	
}

/*
 * créer un élément de liste (LNode)
 * y ranger la donnée (data)
 * insérer l'élément dans la liste *list après l'élément prev
 * ce dernier est supposé appartenir effectivement à la liste
 */
void list_insert_after(List *list, void *data, LNode *curr) {
	
	LNode *n = new_node(data);
	
	if(list->head == NULL)
	{printf("Insertion impossible, liste vide\n" );}
	else
	{
		if(list->tail == curr){list_append(list,data);}
		else{
			n->prev = curr;
			n->next = curr->next;
		
			curr->next->prev = n;
			curr->next = n;
		}
	}
}

/*
 * supprimer le premier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_first(List *list) {

	if(list->size != 0){
		if(list->size==1){
		
			free(list->head->data);
			free(list->head->next);
			free(list->head->prev);
			
			free(list->head);
			free(list->tail);
		}
		else{
			list->head = list->head->next;
			free(list->head->prev->data);
			free(list->head->prev->next);
			free(list->head->prev);
		}
		list->size--;
	}
}

/*
 * supprimer le dernier élément de la liste *list
 * si celle-ci n'est pas vide
 */
void list_remove_last(List *list) {
	
	if(list->size != 0){
		if(list->size > 1){
			list->tail = list->tail->prev;
			
			free(list->tail->next->data);
			free(list->tail->next->prev);
			free(list->tail->next);
		}
		else{
			free(list->head->data);
			free(list->head->next);
			free(list->head->prev);
			
			free(list->head);
			free(list->tail);
		}
		list->size--;
	}
}

/*
 * supprimer l'élément pointé par node de la liste *list
 * l'élément est supposé appartenir effectivement à la liste
 */
void list_remove_node(List *list, LNode *node) {
	
	if(node->prev != NULL){ node->prev->next = node->next;}
	else{ list->head = node->next;}
	
	if(node->next != NULL){	node->next->prev = node->prev;}
	else{ list->tail = node->prev;}
	free(node->data);
	list->size--;
}

/*
 * permute les positions des nœuds curr et curr->next
 * dans la liste list
 */
void list_exchange_curr_next(List *list, LNode *curr) {

  if(list->size == 2){
		curr->next = NULL;
		curr->prev = list->tail;
		curr->prev->next = curr;
		curr->prev->prev = NULL;
		list->head = curr->prev;
		list->tail = curr;
	}
	else if(list->tail == curr->next){
			curr->prev->next = curr->next;
			curr->next->prev = curr->prev;
			curr->next->next = curr;
			curr->prev = curr->next;
			curr->next = NULL;
			list->tail = curr;
		}
		else if(list->head == curr){
				curr->next->next->prev = curr;
				curr->prev = curr->next;
				curr->next = curr->prev->next;
				curr->prev->next = curr;
				curr->prev->prev = NULL;
				list->head = curr->prev;
			}
			else{
				curr->next->next->prev = curr;
				curr->prev->next = curr->next;
				curr->next->prev = curr->prev;
				curr->prev = curr->next;
				curr->next = curr->prev->next;
				curr->prev->next = curr;
			}	
}


/*
 * supprimer tous les éléments de la liste *list
 * sans pour autant supprimer leurs données (data)
 * qui sont des pointeurs
 */
void list_destroy(List *list) {
	
	while (list->size > 0)
	{
		list_remove_first(list);
	}
	
}
