#include <stdlib.h>
#include <stdio.h>
#include "algo.h"
#include "tree.h"

/*
 * ranger dans une liste les segments stockés
 * dans le fichier texte de nom infilename
 * dont la première ligne est un entier indiquant
 * le nombre de segments et les lignes suivantes
 * les coordonnées des segments.
 * chaque ligne est composée de 2 couples.
 * Le premier couple est les coordonnées du début du segment
 * au format : xnum / xden ; pour l'abscisse
 * et : ynum / yden ; pour l'ordonnée.
 * Le second couple est la fin du segment
 */
List * load_segments(char *infilename) {
  List *segments = (List*) calloc(1, sizeof(List));

  FILE *fptr;
  if ((fptr = fopen(infilename,"r")) == NULL) {
    printf("Error while opening file %s.", infilename);
    exit(1);
  }
  int size;
  fscanf(fptr, "%d", &size);
int i;
  for ( i= 0; i < size; i++) {
    long a1, b1, c1, d1, a2, b2, c2, d2;
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a1, &b1, &c1, &d1);
    fscanf(fptr, "%ld/%ld,%ld/%ld", &a2, &b2, &c2, &d2);
    Segment *s = (Segment*) malloc(sizeof(Segment));
    Point p1 = {{a1,b1},{c1,d1}};
    Point p2 = {{a2,b2},{c2,d2}};
    if (point_prec(p1, p2)) {
      (*s).begin = p1;
      (*s).end = p2;
    }
    else {
      (*s).begin = p2;
      (*s).end = p1;
    }
    list_append(segments, s);
  }
  fclose(fptr);
  return segments;
}

/* fonction segment qui prend un LNode en entré 
 * et renvoie le segment qui lui est lié
 * */
Segment * NodeToSeg(LNode * N){
	Segment * S = (Segment *)malloc(sizeof(Segment));
	S = (Segment *)N->data;
	return(S);
}



//renvoie 1 si la liste possede la point p
int ListePossedePoint(List * LI, Point p){
	
	Point Actuel;
	
	LNode * N = (LNode *)malloc(sizeof(LNode));
	N = LI->head;
	
	while(N != NULL){
		Actuel = *((Point *)N->data);
		
		if((eq(Actuel.x,p.x) && eq(Actuel.y,p.y)))
		{return(1);}
		N = N->next;
	}
	return(0);
}





/*
 * ranger dans un fichier texte de nom outfilename
 * les points de la liste *intersections.
 * La premier ligne indique le nombre de points.
 * Puis chaque ligne contient les coordonnée de chaque point.
 * Le format des coordonnées est le même que pour les segments.
 */
void save_intersections(char *outfilename, List *intersections) {
  FILE *fptr;
  if ((fptr = fopen(outfilename,"w")) == NULL) {
    printf("Error while opening file %s.\n", outfilename);
    exit(1);
  }

  fprintf(fptr, "%d\n", intersections->size);
  LNode *curr = intersections->head;
  int i;
  for ( i = 0; i < intersections->size; i++) {
    fprintf(fptr, "%ld/%ld,%ld/%ld\n",
            ((Point*) curr->data)->x.num,
            ((Point*) curr->data)->x.den,
            ((Point*) curr->data)->y.num,
            ((Point*) curr->data)->y.den);
    curr = curr->next;
  }
}

/*
 * exécute l'algorithme glouton sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void allPairs(char *infilename, char *outfilename) {

	List * LS = (List *)malloc(sizeof(List));
	
	LS = load_segments(infilename);
	
	List * L1 = (List *)malloc(sizeof(List));
	List * L2 = (List *)malloc(sizeof(List));
	
	L1->head = LS->head;
	L1->tail = LS->tail->prev;
	L1->size = LS->size - 1;
	
	L2->head = L1->head->next;
	L2->tail = LS->tail;
	L2->size = LS->size;
	
	LNode * N1 = (LNode *)malloc(sizeof(LNode));
	LNode * N2 = (LNode *)malloc(sizeof(LNode));
	
	List * LI = (List *)malloc(sizeof(List));
	LI->head = NULL;
	LI->tail = NULL;
	LI->size = 0;
	
	N1 = L1->head;  
	int i;
	for(i = 0; i < L1->size; i++){
		L2->head = N1->next;
		L2->size -=1;
		N2 = L2->head;
		int j;
		for( j = 1; j <= L2->size; j++){
			if(intersect(*NodeToSeg(N1),*NodeToSeg(N2))){
				list_append(LI,getIntersectionPoint(*NodeToSeg(N1),*NodeToSeg(N2)));
			}
				N2 = N2->next;
		}
		N1 = N1->next;
	}
	save_intersections(outfilename,LI);
	free(N1);
	free(N2);
	free(LS);
	free(L1);
	free(L2);
}	


void TraiteDebSeg(EventTree * E, EventNode * LSA, List * A){
	if(A->size == 0){
		list_append(A,LSA->s1);  // si notre liste de seg actif est vide, on ajoute ce segment 
	}
	else{
		Segment * seg = (Segment *)malloc(sizeof(Segment)); //sinon, on l'ajoute a la bonne place et on regarde si il a un point d'intersection avec l'un de ses voisins, que l'on rajoute a l'arbre des event
		seg = LSA->s1;
		
		int continu = 1;
		
		Segment * segFromList = (Segment *)malloc(sizeof(Segment));
		
		LNode * N = (LNode *)malloc(sizeof(LNode));
		N = A->head;
		
		Rational x = LSA->key.x;
		
		segFromList = NodeToSeg(N);  
		
		if(seg_prec(*seg,*segFromList,x)){ //si notre segment est avant le premier segment de la liste, on le rajoute au debut et on le compare au second
			list_prepend(A,seg);
			if(intersect(*seg,*segFromList)){
				if(!(event_exists(E,*getIntersectionPoint(*seg,*segFromList)))){ //si le point d'intersection existe et qu'il n'est pas déja dans notre arbre d'evenements, on l'ajoute
					insert_event(E,new_event(*getIntersectionPoint(*seg,*segFromList),0,seg,segFromList));
				}
			}
		}else{
			LNode * NPrec = (LNode *)malloc(sizeof(LNode));
			NPrec = A->head;
			N = N->next;
			while(N != NULL && continu == 1){
				segFromList = NodeToSeg(N);
				if(seg_prec(*seg,*segFromList,x)){
					continu = 0;
				}else{
					NPrec = N;
					N = N->next;
				}
			}

			list_insert_after(A,seg,NPrec);
			
			if(intersect(*seg,*NodeToSeg(NPrec))){ //on l'a rajouté a sa place et on le compare au seg precedent (qui existe), puis au seg suivant (s'il existe)
				Point * pInter = (Point *)malloc(sizeof(Point));
				pInter = getIntersectionPoint(*seg,*NodeToSeg(NPrec));
				if(!(event_exists(E,*pInter))){
					insert_event(E,new_event(*pInter,0,seg,NodeToSeg(NPrec)));
				}
			}
			if(N != NULL){
				if(intersect(*seg,*NodeToSeg(N))){
					if(!(event_exists(E,*getIntersectionPoint(*seg,*NodeToSeg(N))))){
						insert_event(E,new_event(*getIntersectionPoint(*seg,*NodeToSeg(N)),0,seg,NodeToSeg(N)));
					}
				}
			}
		}
	}
}


void TraiteFinSeg(EventTree * E, EventNode * LSA, List * A){
	
	int continu = 1;
	
	Point pFin;
	pFin = LSA->key;
	
	Point pActuel;
	
	LNode * N = (LNode *)malloc(sizeof(LNode));
	N = A->head;
	
	
	LNode * NBefore = (LNode *)malloc(sizeof(LNode));
	NBefore = NULL;
	
	while(N != NULL && continu == 1){
		pActuel = NodeToSeg(N)->end;
		if(eq(pActuel.x,pFin.x) && eq(pActuel.y,pFin.y)){
			continu = 0;
		}else{
			NBefore = N;
			N = N->next;
		}
	}
	if(N != NULL && NBefore != NULL && N->next != NULL){
		if(intersect(*NodeToSeg(NBefore),*NodeToSeg(N->next))){
			if(!(event_exists(E,*getIntersectionPoint(*NodeToSeg(NBefore),*NodeToSeg(N->next))))){
				insert_event(E,new_event(*getIntersectionPoint(*NodeToSeg(NBefore),*NodeToSeg(N->next)),0,NodeToSeg(NBefore),NodeToSeg(N->next)));
			}
		}
	}
	
	
	if(N != NULL){ //on l'a bien trouvé
		list_remove_node(A,N);
	}
}


void TraiteIntSeg(EventTree * E, EventNode * LSA, List * A, List * LI){

	Point * inter = (Point *)malloc(sizeof(Point));
	inter = &LSA->key;

	list_append(LI,inter);

	Segment S1 = *LSA->s1;
	Segment S2 = *LSA->s2; 
	
	Point DebS1 = S1.begin;
	Point DebS2 = S2.begin;

	Point Actuel;

	LNode * N = (LNode *)malloc(sizeof(LNode));
	N = A->head;
	
	int compt = 1;
	while(compt && N != NULL){
		Actuel = NodeToSeg(N)->begin;
		if((eq(Actuel.x,DebS1.x) && eq(Actuel.y,DebS1.y)) || (eq(Actuel.x,DebS2.x) && eq(Actuel.y,DebS2.y))){
			compt = 0;
		}else{
			N = N->next;
		}
	}
	
	if(N->prev != NULL){
		if(intersect(*NodeToSeg(N->prev),*NodeToSeg(N->next))){
			if(!(event_exists(E,*getIntersectionPoint(*NodeToSeg(N->prev),*NodeToSeg(N->next)))) && !(ListePossedePoint(LI,*getIntersectionPoint(*NodeToSeg(N->prev),*NodeToSeg(N->next)))))
				{insert_event(E,new_event(*getIntersectionPoint(*NodeToSeg(N->prev),*NodeToSeg(N->next)),0,NodeToSeg(N->prev),NodeToSeg(N->next)));}
		}
	}
	
	if(N->next->next != NULL){
		if(intersect(*NodeToSeg(N),*NodeToSeg(N->next->next))){
			if(!(event_exists(E,*getIntersectionPoint(*NodeToSeg(N),*NodeToSeg(N->next->next))))&& !(ListePossedePoint(LI,*getIntersectionPoint(*NodeToSeg(N),*NodeToSeg(N->next->next)))))
				{insert_event(E,new_event(*getIntersectionPoint(*NodeToSeg(N),*NodeToSeg(N->next->next)),0,NodeToSeg(N),NodeToSeg(N->next->next)));}
		}
	}
	if(N != NULL)
	{list_exchange_curr_next(A,N); }
}


/*
 * exécute l'algorithme Bentley-Ottmmann sur les segments rangés
 * dans le fichier texte de nom infilename et range les points
 * d'intersection dans le fichier texte de nom outfilename
 */
void BentleyOttmmann(char *infilename, char *outfilename) {
	List * LS = (List *)malloc(sizeof(List));
	LS = load_segments(infilename);
	
	List * A = (List *)malloc(sizeof(List));
	A->size = 0;
	A->head = NULL;
	A->tail = NULL;
	
	EventTree * E = (EventTree *)malloc(sizeof(EventTree));
	E->size = 0;
	E->root = NULL;
		
	List * LI = (List *)malloc(sizeof(List));
	LI-> head = NULL;
	LI->tail = NULL;
	LI->size = 0;
	
	EventNode * LSA = (EventNode *)malloc(sizeof(EventNode));
	
	Segment * S = (Segment *)malloc(sizeof(Segment));

	
	for(LNode *N = LS->head; N!=NULL; N = N->next){
		S = NodeToSeg(N);
		
		LSA = new_event(S->begin,1,S,NULL);
		insert_event(E,LSA);
		
		LSA = new_event(S->end,2,S,NULL);
		insert_event(E,LSA);
	 }
	
	while(E->size != 0){
	

		LSA = get_next_event(E);
		switch (LSA->type){
		case 0:
			TraiteIntSeg(E,LSA,A,LI);
			break;
		case 1:
			TraiteDebSeg(E,LSA,A);
			break;
		case 2:
			TraiteFinSeg(E,LSA,A);
			break;
		default:
			printf("type inconnu");
			break;
		}
}
	
	
	save_intersections(outfilename,LI);
}
