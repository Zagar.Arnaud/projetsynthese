#include <stdio.h>
#include <stdlib.h>
#include "geometry.h"

/*
 * affiche le point p
 */
void display_point(const Point p) {
  printf("(");
  display_rational(p.x);
  printf(",");
  display_rational(p.y);
  printf(")");
}

/*
 * affiche le segment s
 */
void display_segment(const Segment s) {
  printf("from: ");
  display_point(s.begin);
  printf(" to: ");
  display_point(s.end);
}

/*
 * renvoie 1 si le point d'intersection key1 précède le point
 * d'intersection key2, 0 sinon
 */
int point_prec(Point key1, Point key2) {
	
return((lt(key1.x,key2.x) == 1) || ((eq(key1.x,key2.x) == 1) && (gt(key1.y,key2.y) == 1)));
}

/*
 * renvoie 1 si s1 précède s2, 0 sinon
 */
int seg_prec(Segment s1, Segment s2, Rational x) {
	
	Rational a1,b1; // s1 a1 X + b1
	
	Rational a2,b2; //s2 a2 X + b2
	
	a1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
	b1 = rsub(s1.begin.y,rmul(a1,s1.begin.x));
		
	a2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
	b2 = rsub(s2.begin.y,rmul(a2,s2.begin.x));
	
	if(!eq(a1,a2)){
		Rational xPointCommun = rdiv(rsub(b2,b1),rsub(a1,a2));
		if((lt(s1.begin.x,xPointCommun) && gt(s1.end.x,xPointCommun) && lt(s2.begin.x,xPointCommun) && gt(s2.end.x,xPointCommun))||(lt(s1.begin.x,xPointCommun) && gt(s1.end.x,xPointCommun) && gt(s2.begin.x,xPointCommun) && lt(s2.end.x,xPointCommun))
		||(gt(s1.begin.x,xPointCommun) && lt(s1.end.x,xPointCommun) && lt(s2.begin.x,xPointCommun) && gt(s2.end.x,xPointCommun))||(gt(s1.begin.x,xPointCommun) && lt(s1.end.x,xPointCommun) && gt(s2.begin.x,xPointCommun) && lt(s2.end.x,xPointCommun)))
			{return(1);}
		else
			{return(0);}
	}
	else
		{return(0);}
}

/*
 * renvoie 1 si s1 et s2 ont une intersection, 0 sinon
 */
int intersect(Segment s1, Segment s2) {
	
	

	Rational a1,b1;// s1  a1 * x + b1
	Rational a2,b2;// s2  a2 * x + b2
	
	a1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
	b1 = rsub(s1.begin.y,rmul(a1,s1.begin.x));
	a2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
	b2 = rsub(s2.begin.y,rmul(a2,s2.begin.x));
	
	
	if(!eq(a1,a2)){
		Rational xPointCommun = rdiv(rsub(b2,b1),rsub(a1,a2));
		
		if((lt(s1.begin.x,xPointCommun) && gt(s1.end.x,xPointCommun) && lt(s2.begin.x,xPointCommun) && gt(s2.end.x,xPointCommun))
		||(lt(s1.begin.x,xPointCommun) && gt(s1.end.x,xPointCommun) && gt(s2.begin.x,xPointCommun) && lt(s2.end.x,xPointCommun))
		||(gt(s1.begin.x,xPointCommun) && lt(s1.end.x,xPointCommun) && lt(s2.begin.x,xPointCommun) && gt(s2.end.x,xPointCommun))
		||(gt(s1.begin.x,xPointCommun) && lt(s1.end.x,xPointCommun) && gt(s2.begin.x,xPointCommun) && lt(s2.end.x,xPointCommun)))
			{return(1);}
		else
			{return(0);}
	}
	else
		{return(0);}

}

/*
 * calcule et renvoie le point d'intersection entre s1 et s2
 */
Point* getIntersectionPoint(Segment s1, Segment s2) {

	Point * pointIntersec = (Point *)malloc(sizeof(Point));
	
	Rational a1,b1;// s1  a1 * x + b1
	Rational a2,b2;// s2  a2 * x + b2

	a1 = rdiv(rsub(s1.end.y,s1.begin.y),rsub(s1.end.x,s1.begin.x));
	b1 = rsub(s1.begin.y,rmul(a1,s1.begin.x));
	
	a2 = rdiv(rsub(s2.end.y,s2.begin.y),rsub(s2.end.x,s2.begin.x));
	b2 = rsub(s2.begin.y,rmul(a2,s2.begin.x));
	
	if(a1.num == 0){
		pointIntersec->x = rdiv(rsub(b1,b2),a2);
		pointIntersec->y = b1;
		
		return(pointIntersec);
	}
	if(a2.num == 0){
		pointIntersec->x = rdiv(rsub(b2,b1),a1);
		pointIntersec->y = b2;
		
		return(pointIntersec);
	}
	
	pointIntersec->x = rdiv(rsub(b2,b1),rsub(a1,a2));
	pointIntersec->y = radd(rmul(a1,pointIntersec->x),b1);
	
	return(pointIntersec);

}
