#include <stdio.h>
#include <stdlib.h>
#include "rational.h"

/*
 * renvoie le PGCD des a et b
 */

static long gcd(long a, long b) {

	long r,q;
	q = a / b;
	r = a - b * q;
	if (r==0) 
		{return b;}
	return (gcd(b,r));
}

/*
 * garantit que:
 * - le dénominateur n'est pas zéro,
 * - le numérateur et le dénominateur sont premiers entre eux,
 * - si le nombre rationnel est négatif, alors le numérateur est négatif
 *   et le dénominateur est positif.
 */
static void simplify(Rational *r) {
	
	if ((*r).den == 0) {
		printf("Rational.simplify: division by zero: ");
		display_rational(*r);
		printf("\n");
		exit(1);
	}

	long d = gcd(labs((*r).num), labs((*r).den));
	(*r).num /= d;
	(*r).den /= d;

	if ((*r).den < 0) {
		(*r).num *= -1;
		(*r).den *= -1;
	}
	

}

/*
 * affiche le nombre rationnel r
 */
void display_rational(const Rational r) {
	printf("%ld/%ld\n", r.num, r.den);
}

/*
 * renvoie a+b
 */
Rational radd(Rational a, Rational b) {
	
	a.num = b.den *a.num;
	b.num = b.num *a.den;
	a.den = b.den *a.den;
	
	
	a.num = a.num + b.num;
	
	if( a.num ) simplify(&a);
	
	return a;
	
	
}

/*
 * renvoie a-b
 */
Rational rsub(Rational a, Rational b) {
	
	a.num = b.den *a.num;
	b.num = b.num *a.den;
	a.den = b.den *a.den;
	
	a.num = a.num - b.num;
	
	if( a.num ) simplify(&a);
	return a;

}

/*
 * renvoie a*b
 */
Rational rmul(Rational a, Rational b) {
	
	a.num = b.num *a.num;
	a.den = b.den *a.den;
	if(a.num!=0){	simplify(&a);}
	return a;
}

/*
 * renvoie a/b
 */
Rational rdiv(Rational a, Rational b) {

	Rational aux;
	aux.num = (a.num * b.den);
	aux.den = (a.den * b.num);
	
	simplify(&aux);

	return aux;
	
}

/*
 * renvoie 1 si a > b,  sinon 0
 */
int gt(Rational a, Rational b) {

	long temp = a.num * b.den;
	long temp2 = a.den * b.num;
	if( temp > temp2)
		{return 1;}
	return 0;
	
}

/*
 * renvoie 1 si a < b,  sinon 0
 */
int lt(Rational a, Rational b) {

	long temp = a.num * b.den;
	long temp2 = a.den * b.num;
	if( temp < temp2)
		{return 1;}
	return 0;
	 
}

/*
 * renvoie 1 si a >= b,  sinon 0
 */
int gte(Rational a, Rational b) {

	long temp = a.num * b.den;
	long temp2 = a.den * b.num;
	if( temp >= temp2){return 1;}
	return 0;
	
}

/*
 * renvoie 1 si a <= b,  sinon 0
 */
int lte(Rational a, Rational b) {
	
	long temp = a.num * b.den;
	long temp2 = a.den * b.num;
	if( temp <= temp2)
	 	{return 1;}
	return 0;
	
}

/*
 * renvoie 1 si a == b,  sinon 0
 */
int eq(Rational a, Rational b) {
	
	int temp = lte(a,b);
	long temp2 = gte(a,b);
	if( temp == temp2)
		{return 1;}
	return 0;

}

/*
 * renvoie 1 si a != b,  sinon 0
 */
int neq(Rational a, Rational b) {

	int temp = lte(a,b);
	long temp2 = gte(a,b);
	if( temp != temp2)
		{return 1;}
	return 0;
	
}

/*
 * renvoie le max entre a et b
 */
Rational max(Rational a, Rational b) {
	
	if (gt(a,b) == 1)
		{return a;}
	else 	
		{return b;}
		
}

/*
 * renvoie le min entre a et b
 */
Rational min(Rational a, Rational b) {
	
	if (lt(a,b) == 1)
		{return a;}
	else 	
		{return b;} 
}
